use <./tcubelib.scad>
include <./tcubeinc.scad>

module tcube_local_noninst(a=1, b=2, c=3) {
    cube([a,b,c]);
}
tcube_used(2,f=2);
